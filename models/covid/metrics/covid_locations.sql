{{ config(materialized = 'view')}}
 select distinct continent, country, location, province, area_km_2_, latitude, longitude, location_level, location_iso_code,
      population, population_density, total_cities, total_districts, 
    total_regencies, total_rural_villages, total_urban_villages
    from {{ref('covid19')}}
    WHERE province is not null