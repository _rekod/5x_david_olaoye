{{ config(materialized = 'table')}}

 select to_date(to_varchar(date::date, 'YYYY-MM-DD')) as report_date, continent, country, sum(case_fatality_rate) as case_fatality_rate,
  sum(case_recovered_rate) as case_recovered_rate from {{ref('covid19')}}
 group by 1, 2, 3 order by 1 desc