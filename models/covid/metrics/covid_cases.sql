{{ config(materialized = 'table')}}

select to_date(to_varchar(date::date, 'YYYY-MM-DD')) as report_date, continent, country, province, sum(new_cases) as new_cases,  sum(new_active_cases) as new_active_cases, 
 sum(total_cases) as total_cases,  sum(total_recovered) as total_recovered
    ,  sum(total_deaths) as total_deaths, sum(case_recovered_rate) as case_recovered_rate, 
    sum(case_fatality_rate) as case_fatality_rate,
    
    sum(total_cases_per_million) as total_cases_per_million,  sum(total_rural_villages) as total_rural_villages,
      sum(new_cases_per_million) as new_cases_per_million,  sum(new_deaths_per_million) as new_deaths_per_million,
      sum(total_deaths_per_million) as total_deaths_per_million
        from {{ref('covid19')}}
        where province is not null
        group by 1,2,3,4
        order by 1 desc