
with
cases as (
    select *
        from {{ ref('covid_cases') }}
),
location as (
    select *
    from {{ ref('covid_locations')}}
),
rates as (
    select * from {{ ref('covid_rates')}}
)
select * from cases
